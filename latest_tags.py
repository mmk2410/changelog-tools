#!/usr/bin/env python
#
# Script for retrieving the git tags in truly chronological order.
#
# 2021 (c) Marcel Kapfer <opensource@mmk2410.org>
# Licensed under the MIT/Expat License
#
# NOTES:
#
# - This is a draft! It may burn your house, delete your harddrive
#   and/or kill your kitten.
#
# ISSUES:
#
# - There are some repos who maintain different major versions over
#   some time in specific branches and that the release not on
#   master/main/whatever but on these version branches. This may
#   result in issues when using the sorting of tags retrieved by this
#   script somewhere else.

import subprocess

def sorted_tags():
    # Retrieve all tags in the repo (that are fetched...)
    tags = subprocess.run(["git", "tag"], stdout=subprocess.PIPE)
    allowedTags = {}
    for tag in tags.stdout.decode('utf-8').splitlines():
        # # Sort out tags that contain any `ignored_keywords`
        # if len([ignored_keyword for ignored_keyword in ignored_keywords if ignored_keyword in tag]) != 0:
        #     continue
        # Retriev the date in ISO-8061 format from git
        date = subprocess.run(["git", "log", "-1", "--format='%aI'", tag], stdout=subprocess.PIPE)
        # Strip away the encloding quotes as well as some whitespace
        allowedTags[tag] = date.stdout.decode('utf-8').strip(" \n'")
    # Sort by keys, which are the retrieved timestampes
    return sorted(allowedTags.items(), key=lambda item: item[1])

def main():
    # # TODO: Add user defined keywords to the list.
    # ignored_keywords = ["pre", "rc", "beta", "alpha", "dev"]
    # # TODO: Add a ability for the user to customize this.
    # default_branch = "main"

    tags = sorted_tags()

    # Printing out
    for k,_ in tags:
        print(k)
    print("Latest tag: {}\nSecond latest tag: {}".format(tags[-1][0],tags[-2][0]))


if __name__ == "__main__":
    main()
