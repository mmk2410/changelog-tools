#!/usr/bin/env python
#
# Script for retrieving the (added) changelog between the last two tags
#
# 2021 (c) Marcel Kapfer <opensource@mmk2410.org>
# Licensed under the MIT/Expat License
#
# NOTES:
#
# - This is a draft! It may burn your house, delete your harddrive
#   and/or kill your kitten.

import sys
import latest_tags
import changelog_diff


def latest_changelog(changelog_file = "./CHANGELOG.md"):
    tags = latest_tags.sorted_tags()
    return changelog_diff.changelog_diff(tags[-2][0], tags[-1][0], changelog_file)


def main():
    help_str = "Must be: ./changelog-diff [changelog file]"
    # Check for correct amount of arguments
    if len(sys.argv) == 1:
        diff = latest_changelog()
    elif len(sys.argv) == 2:
        diff = latest_changelog(sys.argv[1])
    else:
        print("[ERROR] Too much arguments. {}".format(help_str))
        sys.exit(1)
    print("\n".join(diff))


if __name__ == "__main__":
    main()
