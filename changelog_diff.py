#!/usr/bin/env python
#
# Script for retrieving a condensed changelog between two revisions
#
# 2021 (c) Marcel Kapfer <opensource@mmk2410.org>
# Licensed under the MIT/Expat License
#
# NOTES:
#
# - This is a draft! It may burn your house, delete your harddrive
#   and/or kill your kitten.

import sys
import subprocess


def changelog_diff(old, new, changelog_file = "./CHANGELOG.md"):
    raw_diff = subprocess.run(["git", "diff", "--unified=0",
                               "{}..{}".format(old, new), changelog_file],
                              stdout=subprocess.PIPE)
    last_block_start = 0
    diff = []
    for line in raw_diff.stdout.decode('utf-8').splitlines()[4::]:
        if line.startswith("@@ ") and line.endswith(" @@"):
            if last_block_start == 2:
                diff.pop()
            last_block_start = 1
        elif line.startswith("+"):
            last_block_start = last_block_start + 1
            diff.append(line[1::])
        else:
            last_block_start = last_block_start + 1
    return diff


def main():
    help_str = "Must be: ./changelog-diff old-revision new-revision [changelog file]"
    # Check for correct amount of arguments
    if len(sys.argv) < 3:
        print("[ERROR] Not enough arguments. {}".format(help_str))
        sys.exit(1)
    elif len(sys.argv) == 3:
        diff = changelog_diff(sys.argv[1], sys.argv[2])
    elif len(sys.argv) == 4:
        diff = changelog_diff(sys.argv[1], sys.argv[2], sys.argv[3])
    else:
        print("[ERROR] Too much arguments. {}".format(help_str))
        sys.exit(1)
    print("\n".join(diff))


if __name__ == "__main__":
    main()
